#!/usr/bin/python
import RPi_I2C_driver
from time import *
import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306
import Image
import ImageDraw
import ImageFont
import RPi_GPIO
import numpy
import math

# Main LCD Initialization (the 20x4 one)
mainLcd = RPi_I2C_driver.lcd()
mainLcd.backlight(1)
def clc():
	mainLcd.lcd_clear()
def lput(text, x, y):
	mainLcd.lcd_display_string_pos(text, y, x)
# use mainLcd. lcd_display_string_pos lcd_clear() etc...
# Graphing Screen Initialization
graph = Adafruit_SSD1306.SSD1306_128_64(17)
graph.begin()
graph.clear()
# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color
width = graph.width
height = graph.height
image = Image.new('1', (width, height))
draw = ImageDraw.Draw(image)
draw.rectangle((0,0,width,height), outline=0, fill=0)
font = ImageFont.load_default()
# end: blank Graphing screen
# initialize keypad
kp = RPi_GPIO.keypad(columnCount = 3)

def digit():
    # Loop while waiting for a keypress
    r = None
    while r == None:
        r = kp.getKey()
    return r 
# end: initialize keypad
# math functions


def add():
	mainLcd.lcd_display_string_pos("Enter the two numbers to Add", 1, 0)
	mainLcd.lcd_display_string_pos("Enter A: ", 2, 0)
	A=int(digit())
	mainLcd.lcd_display_string_pos("Enter B: ", 3, 0)
	B=int(digit())
	return A + B

def sub():
	mainLcd.lcd_display_string_pos("Enter nums to Sub", 1, 0)
	mainLcd.lcd_display_string_pos("Enter A: ", 2, 0)
	A=int(digit())
	mainLcd.lcd_display_string_pos("Enter B: ", 3, 0)
	B=int(digit())
	return A - B

def mul():
	mainLcd.lcd_display_string_pos("Enter nums to Mult", 1, 0)
	mainLcd.lcd_display_string_pos("Enter A: ", 2, 0)
	A=int(digit())
	mainLcd.lcd_display_string_pos("Enter B: ", 3, 0)
	B=int(digit())
	return A * B

def div():
	mainLcd.lcd_display_string_pos("Enter num to Div", 1, 0)
	mainLcd.lcd_display_string_pos("Enter A: ", 2, 0)
	A=int(digit())
	mainLcd.lcd_display_string_pos("Enter B: ", 3, 0)
	B=int(digit())
	return A / B

def exponent():
	mainLcd.lcd_display_string_pos("Enter base:", 1, 0)
	mainLcd.lcd_display_string_pos("> ", 2, 0)
	A=int(digit())
	mainLcd.lcd_display_string_pos(A, 2, 2)
	mainLcd.lcd_display_string_pos("Enter Power:", 3, 0)
	mainLcd.lcd_display_string_pos("> ", 4, 0)
	B=int(digit())
	mainLcd.lcd_display_string_pos(B, 4, 2)
	return pow(A,B)

def square():
	mainLcd.lcd_display_string_pos("Enter Number:", 1, 0)
	mainLcd.lcd_display_string_pos("> ", 2, 0)
	A=int(digit())
	mainLcd.lcd_display_string_pos(B, 4, 2)
	return sqrt(A)

def xroot():
	mainLcd.lcd_display_string_pos("Enter base:", 1, 0)
	mainLcd.lcd_display_string_pos("> ", 2, 0)
	A=int(digit())
	mainLcd.lcd_display_string_pos(A, 2, 2)
	mainLcd.lcd_display_string_pos("Enter Proot:", 3, 0)
	mainLcd.lcd_display_string_pos("> ", 4, 0)
	B=int(digit())
	mainLcd.lcd_display_string_pos(B, 4, 2)
	return root(A,B)

def logorithm():
	mainLcd.lcd_clear()
	mainLcd.lcd_display_string_pos("1- Base e", 1, 0)
	mainLcd.lcd_display_string_pos("2- Base 10", 2, 0)
	mainLcd.lcd_display_string_pos("3- Base *", 3, 0)
	mainLcd.lcd_display_string_pos("Select Base Style", 4, 0)
	baseStyle = digit()
	if (baseStyle != 3) or (baseStyle != 2) or (baseStyle != 1):
		logorithm()
	A=0
	if baseStyle == 3:
		mainLcd.lcd_display_string_pos("Enter base:", 1, 0)
		mainLcd.lcd_display_string_pos("> ", 2, 0)
		A=int(digit())
		mainLcd.lcd_display_string_pos(A, 2, 2)
	mainLcd.lcd_display_string_pos("Enter X:", 3, 0)
	mainLcd.lcd_display_string_pos("> ", 4, 0)
	B=int(digit())
	mainLcd.lcd_display_string_pos(B, 4, 2)
	returnValue = 0
	if baseStyle == 3:
		returnValue = math.log(B, A)
	elif baseStyle == 2:
		returnValue = log10(B)
	elif baseStyle == 1:
		returnValue = log(B)
	return returnValue
	
def trigin():
	exit()

def degrees():
	exit()


# can I haz merth
def mainCalc():
	fourCalculator = True
	mainLcd.lcd_clear()
	mainLcd.lcd_display_string_pos("1: Addition", 1, 0)
	mainLcd.lcd_display_string_pos("2: Subtraction", 2, 0)
	mainLcd.lcd_display_string_pos("3: Multiplication", 3, 0)
	mainLcd.lcd_display_string_pos("4: Division", 4, 0)
	mainLcd.lcd_display_string_pos("0: Back", 4, 12)
	while fourCalculator:
		CHOICE = digit()
	
		if CHOICE == 1: 
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos('ADDING TWO NUMBERS:', 1, 0)
			result = add()
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos(str(result), 2, 0)
	
		elif CHOICE == 2:
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos('SUBTRACTING TWO NUMS', 1, 0)
			result = sub()
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos(result, 2, 0)
	
		elif CHOICE == 3:
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos('MULTIPLYING TWO NUMS', 1, 0)
			result = mul()
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos(result, 2, 0)

		elif CHOICE == 4:
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos("DIVIDEING TWO NUMS", 1, 0)
			result = div()
			mainLcd.lcd_clear()
			mainLcd.lcd_display_string_pos(result, 2, 0)
	
		elif CHOICE == 0:
			mainLcd.lcd_clear()
			fourCalculator = False

		else:
			mainLcd.lcd_display_string_pos("Wrong Value!", 1, 0)

def menu():
	clc()
	lput("1- Basic Calc", 0, 1)
	lput("2- Log, Exp, Deg", 0, 2)
	lput("3- Triginometry", 0, 3)
	mainMenuChoice = digit()
	if mainMenuChoice == 1:
		mainCalc()
	elif mainMenuChoice == 2:
		clc()
		lput("1- Log", 0, 1)
		lput("2- Exponents", 0, 2)
		lput("3- Degrees", 0, 3)
		menuTwoChoice = digit()
		if menuTwoChoice == 1:
			logorithm()
		elif menuTwoChoice == 2:
			clc()
			lput("1- x^y", 0, 1)
			lput("2- Square Root", 0, 2)
			lput("3- X-Root", 0, 3)
			menu3Choice = digit()
			if menu3Choice == 1:
				exponent()
			elif menu3Choice == 2:
				square()
			elif menu3Choice == 3:
				xroot()
			else:
				clc()
				lput("Incorrect", 4, 2)
				menu()
		elif menuTwoChoice ==3:
			degrees()
	elif mainMenuChoice == 3:
		trigin()
	else:
		clc()
		lput("Incorrect", 4, 2)
		menu()
		




# test
d1 = digit()
d1disp = str(d1)
draw.text((2, 2),    d1disp,  font=font, fill=255)
mainLcd.lcd_display_string_pos(d1disp, 1, 18)
draw.text((2, 22), 'World!', font=font, fill=255)
graph.image(image)
graph.display()
menu()
mainCalc()
# end: test